# Creating local / repository based alias
## Add this to $HOME/.git/config
```
[alias]
        cleanup = ! "git fetch -p && git branch -vv | grep ': gone]' | awk '{ print $1 }' | xargs -r git branch -D"
```
## Command version
```
git config --local alias.ldo "log --graph --decorate --oneline"
```




# Creating global alias
## Add this to $HOME/.gitconfig
```
[alias]
        cleanup = ! "git fetch -p && git branch -vv | grep ': gone]' | awk '{ print $1 }' | xargs -r git branch -D"
```
## Command version
```
git config --global alias.ldo "log --graph --decorate --oneline"
```

# Git aliases tips
```
st = status
ci = commit 
```
[Git Aliases of the Gods! - Git Merge 2017](https://www.youtube.com/watch?v=3IIaOj1Lhb0&ab_channel=GitHub)
