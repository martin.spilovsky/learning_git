# Scenario 1
## Assume the latest commit was already done
## start working on the next patch, and discovered I was missing something

## stash away the current mess I made
`git stash save`

## some changes in the working dir

## and now add them to the last commit:
`git add -u`
`git commit --amend`

## back to work!
`git stash pop`

# Scenario 2
## To save your uncommitted changes
`git stash`

## To list your saved stashes
`git stash list`

## To apply/get back the uncommited changes where x is 0,1,2...
`git stash apply stash@{x}`


## To apply a stash and remove it from the stash list
`git stash pop stash@{x}`

## To apply a stash and keep it in the stash list
`git stash apply stash@{x}`



# Scenario 3
## Part 1:
`git stash (To save your un-committed changes in a "stash". Note: this removes changes from working tree!)`
`git checkout some_branch (change to intended branch -- in this case some_branch)`
`git stash list (list stashes)`

## You can see:
```
stash@{0}: WIP on {branch_name}: {SHA-1 of last commit} {last commit of you branch}
stash@{0}: WIP on master: 085b095c6 modification for test
git stash apply (to apply stash to working tree in current branch)
git stash apply stash@{12} (if you will have many stashes you can choose what stash will apply -- in this case we apply stash 12)
git stash drop stash@{0} (to remove from stash list -- in this case stash 0)
git stash pop stash@{1} (to apply selected stash and drop it from stash list)
```

## Part 2:
You can hide your changes with this command but it is not necessary.
You can continue on the next day without stash.
This commands for hide your changes and work on different branches or for implementation some realisation of your code and save in stashes without branches and commitsor your custom case!
And later you can use some of stashes and check wich is better.

## Part 3:
Stash command for local hide your changes.
If you want work remotely you must commit and push.
