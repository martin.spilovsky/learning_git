# graphical view
```
$  git log --all --graph --decorate --oneline
* 4f1d298 (HEAD -> gerrit_01, origin/gerrit_01) Update dummy_02
| * 604dc8b (origin/gerrit) Update dummy_01
|/
| * d40acf8 (origin/master, origin/HEAD) Update dummy_master
|/
| * 1c7dcaa (origin/gerrit_new_01) Add dummy2
|/
* 0bf5e12 (master, gerrit) all
* d88b4d2 Update README.md
* 01e82ec Update README.md
* 87549d8 Update README.md
* 53c4eee adding cleanup
* a6eeeba Update README.md
* 9b4b573 Update README.md Deleted starting_with_git.md
* 4e57aaa Update starting_with_git.md
*   fee1e79 Merge branch 'martin.spilovsky-master-patch-06601' into 'master'
|\
| * edf8a81 Update starting_with_git
|/
* 811e7a9 Initial commit
```
# some more tips
```
git log --oneline -n 2 # dsiplay last two changes
<<<<<<< HEAD:guidelines/log.md
```
=======
```
>>>>>>> 30db167 (Adding rewriting_history, updating log):guidelines/graph_log.md
