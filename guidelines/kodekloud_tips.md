# Info

git pull = git fetch + git merge

# rebase  
rewrites history but you can also merge several commits into one with the following command use squash  
```
git rebase -i HEAD~4
```

# rebase example
```
sarah (master)$ git log --oneline
5aebaca Merge branch 'master' of http://git.example.com/sarah/story-blog
98da48c Added the wolf and goat story
16afaa3 Added the story index file
ec221af Updated the story index file
16232c8 Added the fox and grapes story
868ed41 Completed frogs-and-ox story
889d20e Add incomplete frogs-and-ox story
2b540e9 Added the lion and mouse story

sarah (master)$ git checkout story/hare-and-tortoise
Switched to branch 'story/hare-and-tortoise'
sarah (story/hare-and-tortoise)$ git log --oneline
2a305a1 Add first draft of hare-and-tortoise story
16afaa3 Added the story index file
868ed41 Completed frogs-and-ox story
889d20e Add incomplete frogs-and-ox story
2b540e9 Added the lion and mouse story

sarah (story/hare-and-tortoise)$ git rebase master
First, rewinding head to replay your work on top of it...
Applying: Add first draft of hare-and-tortoise story

sarah (story/hare-and-tortoise)$ git log --oneline
3e43fed Add first draft of hare-and-tortoise story
5aebaca Merge branch 'master' of http://git.example.com/sarah/story-blog
98da48c Added the wolf and goat story
16afaa3 Added the story index file
ec221af Updated the story index file
16232c8 Added the fox and grapes story
868ed41 Completed frogs-and-ox story
889d20e Add incomplete frogs-and-ox story
2b540e9 Added the lion and mouse story

sarah (story/hare-and-tortoise)$ git log --oneline
91c0c5c Finish hare-and-tortoise
963b99c Update hare-and-tortoise
3e43fed Add first draft of hare-and-tortoise story
5aebaca Merge branch 'master' of http://git.example.com/sarah/story-blog
98da48c Added the wolf and goat story
16afaa3 Added the story index file
ec221af Updated the story index file
16232c8 Added the fox and grapes story
868ed41 Completed frogs-and-ox story
889d20e Add incomplete frogs-and-ox story
2b540e9 Added the lion and mouse story

sarah (story/hare-and-tortoise)$ git rebase -i HEAD~3
[detached HEAD f964e64] Add first draft of hare-and-tortoise story
 Date: Sun Oct 24 09:50:39 2021 +0000
 1 file changed, 20 insertions(+)
 create mode 100644 hare-and-tortoise.txt
Successfully rebased and updated refs/heads/story/hare-and-tortoise.

sarah (story/hare-and-tortoise)$ git log --oneline
f964e64 Add first draft of hare-and-tortoise story
5aebaca Merge branch 'master' of http://git.example.com/sarah/story-blog
98da48c Added the wolf and goat story
16afaa3 Added the story index file
ec221af Updated the story index file
16232c8 Added the fox and grapes story
868ed41 Completed frogs-and-ox story
889d20e Add incomplete frogs-and-ox story
2b540e9 Added the lion and mouse story

```


# git revert
```
git revert commit_id
git reset --soft HEAD~1
git reset --hard HEAD~1

sarah (master)$ git log --oneline
123dd00 Improve story one final time
1580332 Improve story again
502673e Improve story
d2d469b Finish hare-and-tortoise story
2abd8ab Revert "Add author info to stories"
0cd0f5e Add author info to stories
76c2f82 Add stories

sarah (master)$ git reset --hard HEAD~3
HEAD is now at d2d469b Finish hare-and-tortoise story
sarah (master)$ git log --oneline
d2d469b Finish hare-and-tortoise story
2abd8ab Revert "Add author info to stories"
0cd0f5e Add author info to stories
76c2f82 Add stories

```

# git stash
```
git stash
git stash pop
git stash show ID
```

# git reflog
```
git reflog
git reset --hard ID
```