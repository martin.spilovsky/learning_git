# switch to master from feature branch
```
git merge
git mergetool
```

# fix conflicts
```
git merge --continue
```

# remove new-feature branch
```
git branch -D new-feature
```
