# on master
```
git pull
```

# create branch
```
git checkout -b my_cool_feature
```
# edit / add files
```
git add .
git commit -m 'This is a new commit, yay!'
```
# switch to master branch and pull all changes
```
git checkout master
git pull
```
# switch back to feature branch and rebase to master which will merge all master changes to your branch
```
git checkout my_cool_feature
git rebase master
- fix merge conflicts, i've prefer use VSCODE, also use git commit -am "Fixing rebase" first time and them use git commit --amend -am --no-edit
git commit -am "Fixing rebase"
git commit --amend -a --no-edit
- check current status
git status
git rebase --continue
- upload to origin
git push origin my_cool_feature --force # use the force ;) cause Gitlab is just... 
```
# switch to master and rebase 
```
git checkout master
git rebase my_cool_feature
```
# once rebase to master locally is completed without errors, push changes to origin master
```
git push
```

# Example 1
[Source](https://www.ginkgobioworks.com/2020/06/29/gitgo-bioworks-a-tutorial-on-git-and-gitlab/)
```
marti@DESKTOP-RG041FH MINGW64 /e/Gitlab/git4play (master)
$ git branch -a
* master
  remotes/origin/HEAD -> origin/master
  remotes/origin/master

marti@DESKTOP-RG041FH MINGW64 /e/Gitlab/git4play (master)
$ ls
README.md

marti@DESKTOP-RG041FH MINGW64 /e/Gitlab/git4play (master)
$ git checkout -b feature/test01
Switched to a new branch 'feature/test01'

marti@DESKTOP-RG041FH MINGW64 /e/Gitlab/git4play (feature/test01)
$ git commit -am 'update'
[feature/test01 dd940bd] update
 1 file changed, 2 insertions(+), 1 deletion(-)

marti@DESKTOP-RG041FH MINGW64 /e/Gitlab/git4play (feature/test01)
$     git push --set-upstream origin feature/test01
Enumerating objects: 5, done.
Counting objects: 100% (5/5), done.
Writing objects: 100% (3/3), 293 bytes | 293.00 KiB/s, done.
Total 3 (delta 0), reused 0 (delta 0), pack-reused 0
remote:
remote: To create a merge request for feature/test01, visit:
remote:   https://gitlab.com/martin.spilovsky/git4play/-/merge_requests/new?merge_request%5Bsource_branch%5D=feature%2Ftest01
remote:
To https://gitlab.com/martin.spilovsky/git4play.git
 * [new branch]      feature/test01 -> feature/test01
Branch 'feature/test01' set up to track remote branch 'feature/test01' from 'origin'.

marti@DESKTOP-RG041FH MINGW64 /e/Gitlab/git4play (feature/test01)
$

marti@DESKTOP-RG041FH MINGW64 /e/Gitlab/git4play (feature/test01)
$ git status
On branch feature/test01
Your branch is up to date with 'origin/feature/test01'.

nothing to commit, working tree clean

marti@DESKTOP-RG041FH MINGW64 /e/Gitlab/git4play (feature/test01)
$ git checkout master
Switched to branch 'master'
Your branch is up to date with 'origin/master'.

marti@DESKTOP-RG041FH MINGW64 /e/Gitlab/git4play (master)
$ echo "dummy" > new_file

marti@DESKTOP-RG041FH MINGW64 /e/Gitlab/git4play (master)
$ git status
On branch master
Your branch is up to date with 'origin/master'.

Untracked files:
  (use "git add <file>..." to include in what will be committed)
        new_file

nothing added to commit but untracked files present (use "git add" to track)

marti@DESKTOP-RG041FH MINGW64 /e/Gitlab/git4play (master)
$ git add new_file
warning: LF will be replaced by CRLF in new_file.
The file will have its original line endings in your working directory

marti@DESKTOP-RG041FH MINGW64 /e/Gitlab/git4play (master)
$ git status
On branch master
Your branch is up to date with 'origin/master'.

Changes to be committed:
  (use "git restore --staged <file>..." to unstage)
        new file:   new_file


marti@DESKTOP-RG041FH MINGW64 /e/Gitlab/git4play (master)
$ git push
Everything up-to-date

marti@DESKTOP-RG041FH MINGW64 /e/Gitlab/git4play (master)
$ git status
On branch master
Your branch is up to date with 'origin/master'.

Changes to be committed:
  (use "git restore --staged <file>..." to unstage)
        new file:   new_file


marti@DESKTOP-RG041FH MINGW64 /e/Gitlab/git4play (master)
$ git commit -am 'update to master;
> '
[master 1aecb29] update to master;
 1 file changed, 1 insertion(+)
 create mode 100644 new_file

marti@DESKTOP-RG041FH MINGW64 /e/Gitlab/git4play (master)
$ git push
Enumerating objects: 4, done.
Counting objects: 100% (4/4), done.
Delta compression using up to 12 threads
Compressing objects: 100% (2/2), done.
Writing objects: 100% (3/3), 294 bytes | 294.00 KiB/s, done.
Total 3 (delta 0), reused 0 (delta 0), pack-reused 0
To https://gitlab.com/martin.spilovsky/git4play.git
   cf0261d..1aecb29  master -> master

marti@DESKTOP-RG041FH MINGW64 /e/Gitlab/git4play (master)
$ git checkout feature/test01
Switched to branch 'feature/test01'
Your branch is up to date with 'origin/feature/test01'.

marti@DESKTOP-RG041FH MINGW64 /e/Gitlab/git4play (feature/test01)
$ git checkout master
Switched to branch 'master'
Your branch is up to date with 'origin/master'.

marti@DESKTOP-RG041FH MINGW64 /e/Gitlab/git4play (master)
$ git pull
Already up to date.

marti@DESKTOP-RG041FH MINGW64 /e/Gitlab/git4play (master)
$ git checkout feature/test01
Switched to branch 'feature/test01'
Your branch is up to date with 'origin/feature/test01'.

marti@DESKTOP-RG041FH MINGW64 /e/Gitlab/git4play (feature/test01)
$ git rebase -r origin/master
Successfully rebased and updated refs/heads/feature/test01.

marti@DESKTOP-RG041FH MINGW64 /e/Gitlab/git4play (feature/test01)
$ git status
On branch feature/test01
Your branch and 'origin/feature/test01' have diverged,
and have 2 and 1 different commits each, respectively.
  (use "git pull" to merge the remote branch into yours)

nothing to commit, working tree clean

marti@DESKTOP-RG041FH MINGW64 /e/Gitlab/git4play (feature/test01)
$ git status
On branch feature/test01
Your branch and 'origin/feature/test01' have diverged,
and have 2 and 1 different commits each, respectively.
  (use "git pull" to merge the remote branch into yours)

nothing to commit, working tree clean

marti@DESKTOP-RG041FH MINGW64 /e/Gitlab/git4play (feature/test01)
$ git push
To https://gitlab.com/martin.spilovsky/git4play.git
 ! [rejected]        feature/test01 -> feature/test01 (non-fast-forward)
error: failed to push some refs to 'https://gitlab.com/martin.spilovsky/git4play.git'
hint: Updates were rejected because the tip of your current branch is behind
hint: its remote counterpart. Integrate the remote changes (e.g.
hint: 'git pull ...') before pushing again.
hint: See the 'Note about fast-forwards' in 'git push --help' for details.

marti@DESKTOP-RG041FH MINGW64 /e/Gitlab/git4play (feature/test01)
$ git pull
Merge made by the 'recursive' strategy.

marti@DESKTOP-RG041FH MINGW64 /e/Gitlab/git4play (feature/test01)
$ git push
Enumerating objects: 5, done.
Counting objects: 100% (5/5), done.
Delta compression using up to 12 threads
Compressing objects: 100% (3/3), done.
Writing objects: 100% (3/3), 438 bytes | 438.00 KiB/s, done.
Total 3 (delta 1), reused 0 (delta 0), pack-reused 0
remote:
remote: To create a merge request for feature/test01, visit:
remote:   https://gitlab.com/martin.spilovsky/git4play/-/merge_requests/new?merge_request%5Bsource_branch%5D=feature%2Ftest01
remote:
To https://gitlab.com/martin.spilovsky/git4play.git
   dd940bd..d3edb6e  feature/test01 -> feature/test01

marti@DESKTOP-RG041FH MINGW64 /e/Gitlab/git4play (feature/test01)
$ git checkout master
Switched to branch 'master'
Your branch is up to date with 'origin/master'.

marti@DESKTOP-RG041FH MINGW64 /e/Gitlab/git4play (master)
$
```
