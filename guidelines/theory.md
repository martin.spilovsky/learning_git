# GITLAB Rebase, merge, reset, fast-forward and merge conflitcs
[Link](https://www.bogotobogo.com/DevOps/SCM/Git/Git_GitHub_Merge_Conflicts_with_Simple_Example.php)  
[Link](https://www.atlassian.com/git/tutorials/merging-vs-rebasing)  

1. git checkout master
2. git pull
3. git checkout feature_branch
4. git rebase master # moves head of the feature branch after newest master head
5. create merge request from feature_branch to master  

# Fast forward
[Link](https://dev.to/wakeupmh/git-fast-forward-merge-strategy-22j5)
[Link](https://www.bogotobogo.com/DevOps/SCM/Git/Git_GitHub_Fast-Forward_Merge.php)  
[Link](https://medium.com/@mvuksano/git-tips-use-only-fast-forward-merges-with-rebase-c80c9d260a83)  
[Link](https://dev.to/wakeupmh/git-fast-forward-merge-strategy-22j5)

# Rebase vs Merge
```
Short Version
Merge takes all the changes in one branch and merges them into another branch in one commit.
Rebase says I want the point at which I branched to move to a new starting point
So when do you use either one?

Merge
Let's say you have created a branch for the purpose of developing a single feature. When you want to bring those changes back to master, you probably want merge (you don't care about maintaining all of the interim commits).
Rebase
A second scenario would be if you started doing some development and then another developer made an unrelated change. You probably want to pull and then rebase to base your changes from the current version from the repository.


It's simple. With rebase you say to use another branch as the new base for your work.

If you have, for example, a branch master, you create a branch to implement a new feature, and say you name it cool-feature, of course the master branch is the base for your new feature.

Now at a certain point you want to add the new feature you implemented in the master branch. You could just switch to master and merge the cool-feature branch:
```
