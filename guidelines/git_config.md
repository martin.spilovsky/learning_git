# .git/config
```
[core]
        repositoryformatversion = 0
        filemode = false
        bare = false
        logallrefupdates = true
        symlinks = false
        ignorecase = true
        autocrlf = true
[merge]
        conflictstyle = diff3
        tool = meld
[mergetool "meld"]
        path = C:/Program Files (x86)/Meld/Meld.exe
        cmd = \"C:/Program Files (x86)/Meld/Meld.exe\" --diff \"$BASE\" \"$LOCAL\" \"$REMOTE\" --output \"$MERGED\"
        trustExitCode = false
        keepBackup = false
[diff]
        tool = meld
[difftool "meld"]
        cmd = \"C:\\Program Files (x86)\\Meld\\Meld.exe\" "$LOCAL" "$REMOTE"
        path = C:/Program Files (x86)/Meld/Meld.exe

[mergetool]
        keepBackup = false
[alias]
        cleanup = ! "git fetch -p && git branch -vv | grep ': gone]' | awk '{ print $1 }' | xargs -r git branch -D"
```

