# removes local created branches f.e., that were removed from origin
```
$ git fetch -p
From https://gitlab.com/martin.spilovsky/learning_git
 - [deleted]         (none)     -> origin/gerrit_new_01
```
# remove all local stuff
```
$ git reset --hard
HEAD is now at 0bf5e12 all
```
# remove local branch
```
$ git branch -d gerrit_new_01
warning: deleting branch 'gerrit_new_01' that has been merged to
         'refs/remotes/origin/gerrit_new_01', but not yet merged to HEAD.
Deleted branch gerrit_new_01 (was 1c7dcaa).
```
# remove branch on origin
```
git push origin --delete gerrit_new_01
```
# list remote branches and remove old
```
git branch -r
git remote prune origin
```
# wipe
```
git reset --hard
git clean -d -x -f
```