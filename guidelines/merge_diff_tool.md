# Setup MELD
```
git mergetool --tool-help
git difftool --tool-help

git config --global merge.tool meld
git config --global diff.tool meld
git config --global mergetool.meld.path “C:\Program Files (x86)\Meld\meld\meld.exe”

git difftool
git mergetool
```

# .git/config or ~/.gitconfig MELD
```
[merge]
  tool = meld
[mergetool "meld"]
  cmd = \"C:\\Program Files (x86)\\Meld\\Meld.exe\" "$LOCAL" "$BASE" "$REMOTE" "--output=$MERGED"  
  path = C:/Program Files (x86)/Meld/Meld.exe
  trustExitCode = false  
  keepBackup = false

[diff]
  tool = meld
[difftool "meld"]
  cmd = \"C:\\Program Files (x86)\\Meld\\Meld.exe\" "$LOCAL" "$REMOTE"
  path = C:/Program Files (x86)/Meld/Meld.exe
  
[mergetool]
  keepBackup = false
```
# .git/config or ~/.gitconfig P4MERGE
```
[merge]
	tool = p4merge
[mergetool "p4merge"]
	path = C:\\Program Files\\Perforce\\p4merge.exe
[diff]
	tool = p4merge
[difftool "p4merge"]
	path = C:\\Program Files\\Perforce\\p4merge.exe
```