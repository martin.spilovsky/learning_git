# doing cleanup in local repo
```
git branch --merged
git branch --merged | egrep -v "(^\*|master|main|dev)" | xargs git branch -d
```

# Undo last commit but keep file changes
```
git log --oneline
git log --oneline --graph
git reset --soft HEAD~1
```

# Undo last commit and wipe all local changes 
```
git log --oneline
git log --oneline --graph
git reset --HARD HEAD~1
```

# revert a public commit
```
git log --oneline
git log --oneline --graph
git revert HEAD
```

# more help
```
git log --oneline
git checkout <commit hash>
git revert <commit hash>
```

# Reset after bad commit example
```
git checkout alpha-0.3.0 # last know good commit
git reset --hard cc4b63bebb6 # hard reset 
git push origin +alpha-0.3.0 # push back
```

# obvious
```
git checkout --ours PATH/FILE
git checkout --theirs PATH/FILE
```

# going multiple files watch out
```
grep -lr '<<<<<<<' .
grep -lr '<<<<<<<' . | xargs git checkout --ours
grep -lr '<<<<<<<' . | xargs git checkout --theirs
```