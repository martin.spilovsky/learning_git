
[Mermaid live editor, cause diagrams sucks](https://mermaid-js.github.io/mermaid-live-editor/)  
[Kroki, another mermaid live diagram tool](https://kroki.io/)  

```mermaid
graph LR;
  A-->B;
  A-->C;
  B-->D;
  C-->D;
```

```mermaid
gitGraph:
options
{
    "nodeSpacing": 150,
    "nodeRadius": 10
}
end
commit
branch newbranch
checkout newbranch
commit
commit
checkout master
commit
commit
merge newbranch
```
