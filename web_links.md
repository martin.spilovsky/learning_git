# SOURCE

https://charm.cs.illinois.edu/gerrit/Documentation/user-upload.html
https://gerrit-review.googlesource.com/Documentation/intro-user.html
https://www.mediawiki.org/wiki/Gerrit/Tutorial
https://www.freecodecamp.org/news/how-to-delete-a-git-branch-both-locally-and-remotely/

SETUP: https://www.tutorialspoint.com/gerrit/gerrit_quick_guide.htm
TOP: https://www.jondjones.com/architecture/clean-code/code-reviews/absolute-beginners-guide-to-using-gerrit-with-git/
TOP: https://devconnected.com/category/software-engineering/
