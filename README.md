# Learning GIT

tips and guides for git



## Git global setup
```
git config --global user.name "Kenneth"
git config --global user.email "kenneth@xxx.com"
git config --global core.autocrlf true

# nano editor
git config --global core.editor "nano"


# define merge tool
git config merge.tool
git mergetool --tool-help
git config --global merge.tool meld
git config --global merge.tool myfavtool
git config --global merge.conflictStyle diff3 

git config --global diff.tool p4merge
git config --global mergetool.meld.path "C:\Program Files (x86)\Meld\Meld.exe"
git config --global mergetool.keepBackup false


These were set with: ---on Windows---

git config --global core.autocrlf true
git config --global core.safecrlf false
---on Linux---

git config --global core.autocrlf input
git config --global core.safecrlf false
```

## Create a new repository
```
git clone git@gitlab.com:[user name]/[project name].git
cd [project name]
touch README.md
git add README.md
git commit -m "add README"
git push -u origin master
```

# Saving KEY into WLS Windows / Linux
```
git config credential.helper store
# then use git pull
lolo:~$ cat .git-credentials
USERNAME:PWD
lolo:~$
```

## Clone repo
```
git clone git@gitlab.com:martin.spilovsky/my_home_projects.git
```  

### Branch commands
```
git branch
git branch -a
```

### Check status
```
git status
```

### Add new stuff, found out whats new with git status
```
git add folder/filename
```

### Remove a file
```
git rm --cached folder/filename
```

### Add all new
```
git add .
```

### After doing git add, commit stuff
```
git commit -m 'Adding currency convertor'
```

### Add and commit
```
git commit -am "Update files"
```

### Creating gitignore
```
tee .gitignore << EOF
.*
!/.gitignore
EOF

git commit -am "Adding gitignore"
git push

# in case you have already files which you are working with and then you want to define gitignore
git commit -am 'my files'
git rm -r --cached .
git add .
git commit -am 'applying git ignore'
```

### Before doing any changes make a git pull or git pull origin master
```
git pull origin master
```

### Push changes to origin master or to the default branch find out with git branch -a
```
git push origin master
git push -u  origin feature/branch # for creating a new feature branch on origin
git push origin feature/branch or just git push
```

### Show default pull / push
```
git remove -v
```

### Recover deleted file if it was before tagged + commit + push
```
git checkout -- file-name
```

### Scenario 1, pull all from master, create own branch and switch to it, do some stuff + commit them and create branch on origin
```
git pull
git checkout -b git_feature_branch_01
git commit -m 'Adding ansible tips'
git push --set-upstream git_feature_branch_01 or use git push -u origin git_feature_branch_01 
git branch ||  git branch -r  || git branch -a

-- cleanup 
git branch -d git_feature_branch_01
git push origin --delete origin/git_feature_branch_01
git fetch -p --> sync from local to remote
```
### Update origin url
```
git remote set-url origin https://gitlab.com/URI/something.git
```
